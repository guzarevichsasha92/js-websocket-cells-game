import Observer from './Observer';
import AppActions from '../types/AppActions';

class AppView extends Observer {
  constructor(controller) {
    super();

    this.controller = controller;

    this.root = document.getElementById('app');

    this.refs = {
      dialog: 'main-dialog',
      info: {
        clientId: 'info-client-id',
        gameId: 'info-game-id',
        clients: 'info-game-clients',
      },
      game: {
        inputBalls: 'input-balls',
        newGame: 'new-game-btn',
        joinGame: 'join-game-btn',
        inputGameId: 'input-game-id',
        gamesList: 'games-list',
        board: 'board',
      },
    };

    this.render();

    this.root.addEventListener('click', controller);

    this.controller.model.addObserver(this);
  }

  update({ action, model }) {
    if (action === AppActions.UPDATE_INFO_CLIENT_ID) {
      this.#updateInfoClientId(model.clientId);

      this.#renderGamesList(model.games);
    }

    if (action === AppActions.UPDATE_INFO_GAME_ID) {
      this.#updateInfoGameId(model.gameId);
    }

    if (action === AppActions.UPDATE_GAMES) {
      this.#renderGamesList(model.games);
    }

    if (action === AppActions.JOIN_GAME) {
      this.#toggleDialog(model.dialog);

      this.#updateInfoGameId(model.gameId);

      this.#renderClients(model.clients);

      this.#renderBoard(model.balls);
    }

    if (action === AppActions.DO_PLAYER_MOVE) {
      // DO_PLAYER_MOVE action contains model itself inside model.self
      this.#repaintBall(model.id, model.color);
    }

    if (action === AppActions.DO_OPPONENT_MOVE) {
      // DO_PLAYER_MOVE action contains model itself inside model.self
      this.#repaintBall(model.id, model.color);
    }
  }

  #updateInfoClientId(clientId) {
    this.root.querySelector(`#${this.refs.info.clientId}`).textContent =
      clientId;
  }

  #updateInfoGameId(gameId) {
    this.root.querySelector(`#${this.refs.info.gameId}`).textContent = gameId;
  }

  #toggleDialog(value) {
    const dialog = this.root.querySelector(`#${this.refs.dialog}`);

    if (value) {
      dialog.classList.remove('hidden');
      dialog.classList.add('block');
    } else {
      dialog.classList.remove('block');
      dialog.classList.add('hidden');
    }
  }

  #renderBoard(balls) {
    const board = this.root.querySelector(`#${this.refs.game.board}`);

    while (board.firstChild) {
      board.removeChild(board.firstChild);
    }

    for (let i = 0; i < balls; i++) {
      const button = document.createElement('button');

      button.id = `ball-${i + 1}`;
      button.textContent = i + 1;
      button.className =
        'w-32 h-32 rounded-full text-sm p-4 font-semibold border-2 border-slate-500 shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2';

      board.appendChild(button);
    }
  }

  #repaintBall(id, color) {
    const ball = this.root.querySelector(`#${id}`);
    const prefix = 'bg';
    const regex = new RegExp('\\b' + prefix + '[^ ]*[ ]?\\b', 'g');

    if (ball.className.match(regex)) {
      ball.className = ball.className.replaceAll(regex, color);
    } else {
      ball.classList.add(color);
    }
  }

  #renderClients(clients) {
    const list = this.root.querySelector(`#${this.refs.info.clients}`);

    while (list.firstChild) {
      list.removeChild(list.firstChild);
    }

    clients.forEach((client, index) => {
      const li = document.createElement('li');

      li.className = 'text-lg p-2 font-semibold flex items-center';
      li.innerHTML = `
        <div class="w-4 h-4 rounded-full mr-4 ${client.color}"></div>
        ${
          client.clientId === this.controller.model.clientId
            ? 'You'
            : 'Player' + (index + 1)
        }
        `;

      list.appendChild(li);
    });
  }

  #renderGamesList(games) {
    const list = this.root.querySelector(`#${this.refs.game.gamesList}`);

    while (list.firstChild) {
      list.removeChild(list.firstChild);
    }

    for (const gameId of Object.keys(games)) {
      const { balls, clients, id } = games[gameId];

      const li = document.createElement('li');

      li.className = 'flex flex-col sm:flex-row justify-between p-2 w-100';

      li.innerHTML = `
          <h4 class="text-sm font-medium leading-6 text-gray-900">${id}</h4>

          <p class="text-sm font-medium leading-6 text-gray-900">${balls} balls</p>

          <p class="text-sm font-medium leading-6 text-gray-900">${clients.length} / 3</p>
        `;

      list.append(li);
    }
  }

  render() {
    this.root.innerHTML = `
    <main class="bg-white p-4 grid grid-cols-3 gap-3 w-full">
        <aside class="col-span-3 md:col-span-1 bg-slate-100 rounded p-2">
          <div class="px-4 sm:px-0">
            <h3 class="text-base font-semibold leading-7 text-gray-900">
              Player info
            </h3>
          </div>

          <div class="mt-4 border-t border-b border-gray-100">
            <dl class="divide-y divide-gray-100">
              <div class="px-2 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt class="text-sm font-medium leading-6 text-gray-900">
                  clientId
                </dt>
                <dd
                  id="${this.refs.info.clientId}"
                  class="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0"
                >
                  ${this.controller.model.clientId}
                </dd>
              </div>

              <div class="px-2 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt class="text-sm font-medium leading-6 text-gray-900">
                  gameId
                </dt>
                <dd
                  id="${this.refs.info.gameId}"
                  class="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0"
                >
                    ${this.controller.model.gameId}
                </dd>
              </div>
            </dl>
          </div>

          <div class="px-4 sm:px-0 mt-4">
            <h3 class="text-base font-semibold leading-7 text-gray-900">
              Clients
            </h3>
          </div>

          <div class="mt-4 border-t border-gray-100">
            <ul
              id="${this.refs.info.clients}"
              role="list"
              class="divide-y divide-gray-100 rounded-md border border-gray-200"
            >
            </ul>
          </div>
        </aside>

        <section class="col-span-3 md:col-span-2">
          <div
            id="${this.refs.game.board}"
            class="flex flex-wrap"
          ></div>
        </section>
      </main>

      <div
        id="${this.refs.dialog}"
        class="relative z-10"
        aria-labelledby="modal-title"
        role="dialog"
        aria-modal="true"
      >
        <div
          class="fixed inset-0 bg-gray-900 bg-opacity-75 transition-opacity"
        ></div>

        <div class="fixed inset-0 z-10 overflow-y-auto">
          <div
            class="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0"
          >
            <div
              class="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg"
            >
              <div class="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                <div class="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left">
                  <div class="flex flex-row justify-between items-center mb-4">
                    <ul class="flex flex-row justify-evenly list-none p-0 m-0 w-full">
                        <li class="bg-rose-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-cyan-500 w-2 h-2 rounded-full"></li>
                        <li class="bg-yellow-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-violet-500 w-2 h-2 rounded-full"></li>
                        <li class="bg-lime-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-orange-500 w-2 h-2 rounded-full"></li>
                        <li class="bg-purple-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-fuchsia-500 w-2 h-2 rounded-full"></li>
                        <li class="bg-slate-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-pink-500 w-2 h-2 rounded-full"></li>
                    </ul>

                    <h3
                        class="text-xl sm:text-3xl text-center w-full text-gray-900"
                        id="modal-title"
                    >
                        Cell Game
                    </h3>

                    <ul class="flex flex-row justify-between list-none p-0 m-0 w-full">
                        <li class="bg-indigo-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-teal-500 w-2 h-2 rounded-full"></li>
                        <li class="bg-zinc-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-red-500 w-2 h-2 rounded-full"></li>
                        <li class="bg-gray-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-sky-500 w-2 h-2 rounded-full"></li>
                        <li class="bg-emerald-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-stone-500 w-2 h-2 rounded-full"></li>
                        <li class="bg-green-500 w-2 h-2 rounded-full animate-pulse"></li>
                        <li class="bg-neutral-500 w-2 h-2 rounded-full"></li>
                        <li class="bg-blue-500 w-2 h-2 rounded-full animate-pulse"></li>
                    </ul>
                  </div>
                  
                  <div class="flex flex-col">
                    <div class="mb-4">
                        <label for="balls" class="block text-sm font-medium leading-6 text-gray-900">
                          Balls
                        </label>
                        
                        <input id="${this.refs.game.inputBalls}" type="number" name="balls" class="block w-full rounded-md border-0 p-2 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" placeholder="20">
                    </div>

                    <button
                      id="${this.refs.game.newGame}"
                      class="rounded-md bg-indigo-600 px-3.5 py-2.5 mb-4 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                    >
                      New Game
                    </button>

                    <div
                      class="grid grid-flow-row grid-cols-3 gap-3 content-center mb-4"
                    >
                      <input
                        type="text"
                        name="gameId"
                        id="${this.refs.game.inputGameId}"
                        class="col-span-2 mt-1 block w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0"
                        placeholder="Paste game id"
                      />

                      <button
                        id="${this.refs.game.joinGame}"
                        class="col-span-1 rounded-md bg-teal-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-teal-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-teal-600"
                      >
                        Join Game
                      </button>
                    </div>

                    <ul id="${this.refs.game.gamesList}" class="list-none p-0 m-0 divide-y divide-gray-100 rounded-md border border-gray-200">
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

export default AppView;
