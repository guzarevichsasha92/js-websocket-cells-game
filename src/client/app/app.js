import AppModel from './model/AppModel';
import AppController from './controller/AppController';
import AppView from './view/AppView';

export default class App {
  constructor() {
    this.model = new AppModel();
    this.controller = new AppController(this.model);
    this.view = new AppView(this.controller);
  }
}
