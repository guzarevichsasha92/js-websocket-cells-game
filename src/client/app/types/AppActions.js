export default {
  UPDATE_INFO_CLIENT_ID: 'UPDATE_INFO_CLIENT_ID',
  UPDATE_INFO_GAME_ID: 'UPDATE_INFO_GAME_ID',
  UPDATE_INFO_BALLS: 'UPDATE_INFO_BALLS',
  UPDATE_DIALOG: 'UPDATE_DIALOG',
  CREATE_BOARD: 'CREATE_BOARD',
  JOIN_GAME: 'JOIN_GAME',
  UPDATE_GAMES: 'UPDATE_GAMES',
  DO_PLAYER_MOVE: 'DO_PLAYER_MOVE',
  DO_OPPONENT_MOVE: 'DO_OPPONENT_MOVE',
};
