/**
 * Content server installation
 */
const express = require('express');
const viteExpress = require('vite-express');

const app = express();

app.get('/hello', (req, res) => {
  res.send('Hello Vite!');
});

viteExpress.listen(app, 9091, () =>
  console.log('Server is listening on port 9091...')
);

//**
//* Websocket server installation
//*/
const http = require('http');
const websocket = require('websocket');

const websocketServer = websocket.server;
const httpServer = http.createServer();
httpServer.listen(9090, () =>
  console.log('Websocket is listening on port 9090...')
);

const wsServer = new websocketServer({
  httpServer: httpServer,
});

/**
 * Websocket messages handling
 */
const { generateGUID } = require('./utils');

//hashmap clients
const clients = {};
const games = {};

wsServer.on('request', (request) => {
  //connect
  const connection = request.accept(null, request.origin);

  connection.on('open', () => console.log('opened!'));

  connection.on('close', () => console.log('closed!'));

  connection.on('message', (message) => {
    const result = JSON.parse(message.utf8Data);
    // I have received a message from the client

    // a user want to create a new game
    if (result.method === 'create') {
      const clientId = result.clientId;
      const balls = result.balls;

      const gameId = generateGUID();

      games[gameId] = {
        id: gameId,
        balls: balls || 20,
        clients: [],
      };

      const payLoad = {
        method: 'create',
        game: games[gameId],
      };

      const con = clients[clientId].connection;
      con.send(JSON.stringify(payLoad));

      broadcastGameList();
    }

    // a client want to join
    if (result.method === 'join') {
      const clientId = result.clientId;
      const gameId = result.gameId;
      const game = games[gameId];

      console.log(
        `A client wants to join game where there are aleady ${game.clients.length} clients`
      );

      if (game.clients.length >= 3) {
        // sorry max players reach
        console.log(`game ${gameId} reached max clients`);
        return;
      }

      const color = {
        0: 'bg-rose-500',
        1: 'bg-cyan-500',
        2: 'bg-yellow-500',
        3: 'bg-violet-500',
        4: 'bg-lime-500',
        5: 'bg-orange-500',
        6: 'bg-purple-500',
        7: 'bg-fuchsia-500',
        8: 'bg-slate-500',
        9: 'bg-pink-500',
        10: 'bg-indigo-500',
        11: 'bg-teal-500',
        12: 'bg-zinc-500',
        13: 'bg-red-500',
        14: 'bg-gray-500',
        15: 'bg-sky-500',
        16: 'bg-emerald-500',
        17: 'bg-stone-500',
        18: 'bg-green-500',
        19: 'bg-neutral-500',
        20: 'bg-blue-500',
      }[game.clients.length];

      game.clients.push({
        clientId: clientId,
        color: color,
      });

      // start the game
      if (game.clients.length === 3) {
        updateGameState();
      }

      const payLoad = {
        method: 'join',
        game: game,
      };
      // loop through all clients and tell them that people has joined
      game.clients.forEach((c) => {
        clients[c.clientId].connection.send(JSON.stringify(payLoad));
      });

      broadcastGameList();
    }

    // a user plays
    if (result.method === 'play') {
      const gameId = result.gameId;
      const ballId = result.ballId;
      const color = result.color;
      let state = games[gameId].state ?? {};

      state[ballId] = color;
      games[gameId].state = state;
    }
  });

  // generate a new clientId
  const clientId = generateGUID();
  clients[clientId] = {
    connection: connection,
  };

  const payLoad = {
    method: 'connect',
    clientId: clientId,
    games, // already created games
  };
  // send back the client connect
  connection.send(JSON.stringify(payLoad));
});

function broadcastGameList() {
  for (const clientId of Object.keys(clients)) {
    const payLoad = {
      method: 'update-game-list',
      games,
    };

    clients[clientId].connection.send(JSON.stringify(payLoad));
  }
}

function updateGameState() {
  for (const g of Object.keys(games)) {
    const game = games[g];
    const payLoad = {
      method: 'update',
      game: game,
    };

    game.clients.forEach((c) => {
      clients[c.clientId].connection.send(JSON.stringify(payLoad));
    });
  }

  setTimeout(updateGameState, 500);
}
